package com.example.p2_de_edgar_ruiz;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class HelloController2_2 {
    @FXML
    private Button btnConvert;

    @FXML
    private CheckBox auto;

    @FXML
    private Label lbNumero;

    @FXML
    private Slider slider;

    @FXML
    private TextField txtInput, txtOutput;
    private double valorSalida;


    @FXML
    void convertBtn(ActionEvent event) {
        if(!auto.isSelected()){
            valorSalida = slider.getValue() * Double.valueOf(txtInput.getText());
            txtOutput.setText(String.valueOf(Math.round(valorSalida*100.0)/100.0));
        }
    }

    @FXML
    void clearBtn(ActionEvent event) {
        txtInput.setText("");
        txtOutput.setText("");
    }

    @FXML
    void checkBox(ActionEvent event) {
        if(auto.isSelected()){
            btnConvert.setDisable(true);
        }else{
            btnConvert.setDisable(false);
        }
    }
}
