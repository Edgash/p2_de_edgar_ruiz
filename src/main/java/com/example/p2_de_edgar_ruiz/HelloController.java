package com.example.p2_de_edgar_ruiz;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class HelloController {
    @FXML
    private Label labelNum;
    @FXML
    private Label operacion;
    @FXML
    private TextField txtBox;
    @FXML
    private CheckBox restar;

    private int total;

    @FXML
    protected void clickButton(ActionEvent event) {
        String opcion = ((Button) event.getSource()).getText();

        String textoLabel = labelNum.getText ();
        int numLabel = Integer.parseInt(textoLabel);

        if(restar.isSelected()){
            switch(opcion){
                case "1":
                    numLabel = numLabel - 1;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
                case "5":
                    numLabel = numLabel - 5;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
                case "10":
                    numLabel = numLabel - 10;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
            }
        }else {
            switch (opcion) {
                case "1":
                    numLabel = numLabel + 1;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
                case "5":
                    numLabel = numLabel + 5;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
                case "10":
                    numLabel = numLabel + 10;
                    labelNum.setText(String.valueOf(numLabel));
                    break;
            }
        }
    }

    @FXML
    protected void clickSuma(ActionEvent event) {
        String numeroString = txtBox.getText ();
        int numero = Integer.parseInt(numeroString);

        String textoLabel = labelNum.getText ();
        int numLabel = Integer.parseInt(textoLabel);



        if(restar.isSelected()) {
            total = numLabel - numero;
            labelNum.setText(String.valueOf(total));
        }else{
            total = numLabel + numero;
            labelNum.setText(String.valueOf(total));
        }
    }

    @FXML
    protected void checkBox(ActionEvent event){
        if(restar.isSelected()) {
            operacion.setText("¡Estás restando!");
        }else{
            operacion.setText("¡Estás sumando!");
        }
    }
}